const express = require("express");
const router = express.Router(); 
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js")

// CREATE A PRODUCT

router.post("/create", auth.verify, (req, res) => {

	const data = {
	product: req.body, 
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}

	if (data.isAdmin) {
	productController.createProduct(data.product).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false);
	}
})

// RETRIEVE ALL PRODUCTS
router.get("/all", (req,res) => {

	productController.allProducts().then(resultFromController => res.send(resultFromController));
})


// RETRIEVE ALL ACTIVE PRODUCTS
 router.get("/active", (req, res) => {

 	productController.activeProducts().then(resultFromController => res.send(resultFromController));
 })

 // [SESSION 44]
 // RETRIEVE A SINGLE PRODUCT

 router.get("/:productId", (req, res) => {

 	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

 })

 // UPDATE PRODUCT INFORMATION (admin only)

 router.patch("/update/:productId", auth.verify, (req, res) => {

 	const data = {

 		product: req.body,
 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
 		params: req.params
 	}

 	if (data.isAdmin) {

 		productController.updateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));

 	} else {

 		res.send(false);
 	}

 })

 // ARCHIVE PRODUCT(Admin Only)
 router.put("/archive/:productId", auth.verify, (req, res) => {

 	const data = {

 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
 		params: req.params
 	}

 	if (data.isAdmin) {

 		productController.archiveProduct(data.params).then(resultFromController => res.send(resultFromController));
 	
 	} else {

 		res.send(false);
 	}
 })

// unarchive

 router.put("/unarchive/:productId", auth.verify, (req, res) => {

    const data = {

        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        params: req.params
    }

    if (data.isAdmin) {

        productController.unarchiveProduct(data.params).then(resultFromController => res.send(resultFromController));
    
    } else {

        res.send(false);
    }
 })

 module.exports = router;

