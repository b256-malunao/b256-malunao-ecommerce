const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js")

router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// Router for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Router for user authentication
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController))

})



// RETRIEVE USER DETAILS
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});

// router.post("/checkout", auth.verify, (req, res) => {

// 	const data = {
// 	productId: req.body.productId,
// 	quantity: req.body.quantity,
// 	productName: req.body.productName,
// 	userId: auth.decode(req.headers.authorization).id,
// 	isAdmin: auth.decode(req.headers.authorization).isAdmin,
// }

// 	if (data.isAdmin === false) {
// 	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
	
// 	} else {

// 		res.send(false);
// 	}
// })



router.post("/checkout", auth.verify, (req, res) => {

	const data = {
	productId: req.body.productId,
	quantity: req.body.quantity,
	productName: req.body.productName,
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
}

	if (data.isAdmin === false) {
	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);
	}
})

// [STRETCH GOALS]
// RETRIEVE ALL ORDERS(admin only)
router.get("/orders", auth.verify, (req, res) => {

 	const data = {
 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
 		params: req.params
 	}

 	if (data.isAdmin) {

 		userController.retrieveAllOrders(data.params).then(resultFromController => res.send(resultFromController));
 	
 	} else {

 		res.send(false);
 	}
 })




// Retrieve Authenticated User's Order (Non-admin only)
/*router.get("/myOrders", auth.verify, (req, res) => {

	const data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
}

	if (data.isAdmin === false) {
	userController.retrieveOrder(data).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);
	}


})*/

router.get("/myOrders/:userId", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		}

	if(data.isAdmin) {

		res.send(false);

	} else {

	userController.retrieveOrder(data).then(resultFromController => res.send(resultFromController));

	}
});


// Set User as Admin Functionality

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	params: req.params
}

	if (data.isAdmin) {
	userController.setAsAdmin(data.params).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);
	}
})

module.exports = router;