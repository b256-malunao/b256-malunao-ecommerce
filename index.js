// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");

// SETUP SERVER
const app = express();

// MIDDLEWARE
app.use(cors()); 
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// Database connection
mongoose.connect("mongodb+srv://admin:admin1234@b256malunao.ob1pdkd.mongodb.net/B256_EcommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));




// Server Listening
app.listen(4000, () => console.log(`API is now online on port 4000`));