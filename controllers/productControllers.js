const Product = require("../models/Product.js");
const auth = require("../auth.js");


// CREATE A PRODUCT
module.exports.createProduct = (product) => {

let newProduct = new Product ({

		name: product.name,
		description: product.description,
		price: product.price,
		image: product.image
	});

	return newProduct.save().then((result, err) => {
		if(err) {
			
			return false;
		
		} else {

			return true;
		};
	});
	
}


// RETRIEVE ALL PRODUCTS
module.exports.allProducts = () => {

	return Product.find({}).then(result => {
		
		return result;

	});
} 

// RETRIEVE ALL ACTIVE PRODUCTS
module.exports.activeProducts = () => {

	return Product.find({isActive: true}).then(result => {

		return result;
	});
}

// [SESSION 44]
 // RETRIEVE A SINGLE PRODUCT

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result;
	})
}

 // UPDATE PRODUCT INFORMATION (admin only)
 module.exports.updateProduct = (product, paramsId) => {

 	let updatedProduct = {
		name: product.name,
		description: product.description,
		price: product.price,
		image: product.image
	}

 	return Product.findByIdAndUpdate(paramsId.productId, updatedProduct).then((result, err) => {

		if(err) {
			
			return false;
		
		} else {

			return true
		}
	})
}

 // ARCHIVE PRODUCT(Admin Only)

module.exports.archiveProduct = (paramsId) => {

let archivedActiveField = {
		isActive: false
	}

	return Product.findByIdAndUpdate(paramsId.productId, archivedActiveField).then((result, err) => {

		if (err) {

			return false;

		} else {

			return true
		}
	})
}

module.exports.unarchiveProduct = (paramsId) => {

let archivedActiveField = {
		isActive: true
	}

	return Product.findByIdAndUpdate(paramsId.productId, archivedActiveField).then((result, err) => {

		if (err) {

			return false;

		} else {

			return true
		}
	})
}