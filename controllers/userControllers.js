const User = require("../models/User.js");

const Product = require("../models/Product.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Check Email
module.exports.checkEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {

		if(result.length > 0) {

			return true;

		} else {

			return false;
		}
	})
};

// User Registration
module.exports.registerUser = (requestBody) => {

	let newUser = new User({

		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err) {

			return false;
		
		} else {
		
			return true;
		
		}
	})
}

// User authentication
module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null) {

			return false;
		
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if(isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}
			
			} else {

				return false;
			}

		}
	})

}

// RETRIEVE USER DETAILS
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		
		result.password = "";

		return result;

	});

};

// NON-ADMIN USER CHECKOUT (Create Order)





module.exports.createOrder = async (data) => {

let price = await Product.findById(data.productId).then(product => {
					return product.price
				});
let totalProductAmount = price * data.quantity;


	let isUserUpdated = await User.findById(data.userId).then(user => {
		
		user.orderedProducts.push({
			
			// products: {
				productId: data.productId,
				productName: data.productName,
				quantity: data.quantity,
			// },
			totalAmount: totalProductAmount

		});
		


		return user.save().then((checkedOut, err) => {

			if(err) {

				return false;

			} else {

				return true

			}
		})
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.userOrders.push({
			userId: data.userId
		});

		return product.save().then((ordered, err) => {

			if(err) {

				return false;

			} else {

				return true;

			}
		})
	});

	if(isUserUpdated && isProductUpdated) {

		return true;

	} else {

		return false;

	}
}

// RETRIEVE ALL ORDERS(admin only)

module.exports.retrieveAllOrders = () => {


	return User.find({}).then(result => {

	let orders = result.map(order => order.orderedProducts);
		return orders;
	});	
}

// Retrieve Authenticated User's Order (Non-admin only)

/*module.exports.retrieveOrder = (data) => {
	
	User.findById(data.userId).then(result => {

		return result;
	})

};*/

module.exports.retrieveOrder = (data) => {

	return User.findById(data.userId).populate('orderedProducts').then(order => {

		return order.orderedProducts;

	});
};


// Set User as Admin Functionality
module.exports.setAsAdmin = (paramsId) => {

let makeAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(paramsId.userId, makeAdmin).then((result, err) => {

		if (err) {

			return false;

		} else {

			return true
		}
	})
}
