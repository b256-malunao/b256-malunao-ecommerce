//// Setup dependencies
const mongoose = require("mongoose");

// Create the Schema
const userSchema = new mongoose.Schema({
	
	firstName: {
		type: String,
		required: [true, "Firstname is required"]
	},
	lastName: {
		type: String,
		required: [true, "Lastname is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false 
	},

	orderedProducts: [

		{	

			productId: {
				type: String,
				required: [true, "Product ID is required"]
						},
			productName: {
				type: String,
				required:  [true, "Product Name is required"]
						}, 
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
						},
			totalAmount: {
				type: Number,
				required: [true, "Total Amount is required"]
						},
			purchasedOn: {
				type: Date,
				default: new Date()
						}
		}

	]

});



module.exports = mongoose.model("User", userSchema);

