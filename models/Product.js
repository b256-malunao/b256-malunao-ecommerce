// Setup dependencies
const mongoose = require("mongoose");

// Create the Schema
const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product Name is required"]
	},
	description: {
		type: String,
		required: [true, "Product description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	image: {
		type: String,
		default: "https://pixy.org/images/placeholder.png"
	},
	createdOn: {
		type: Date,
	 	default: new Date()
	},
	userOrders: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},

			orderId: {
				type: String
			}
			
		}	
	]

});


module.exports = mongoose.model("Product", productSchema);

